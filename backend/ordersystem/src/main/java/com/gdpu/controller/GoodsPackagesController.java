package com.gdpu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
@Controller
@RequestMapping("/goodsPackages")
public class GoodsPackagesController {

}

