package com.gdpu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hjs
 * @since 2021-01-24
 */
@RestController
@RequestMapping("/tbUser")
public class TbUserController {

}

