package com.gdpu.service;

import com.gdpu.bean.Leftmenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjs
 * @since 2021-02-22
 */
public interface LeftmenuService extends IService<Leftmenu> {

}
