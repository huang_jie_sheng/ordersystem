package com.gdpu.service;

import com.gdpu.bean.Count;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjs
 * @since 2021-02-14
 */
public interface CountService extends IService<Count> {

}
