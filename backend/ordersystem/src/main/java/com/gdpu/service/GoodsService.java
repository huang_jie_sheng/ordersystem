package com.gdpu.service;

import com.gdpu.bean.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
public interface GoodsService extends IService<Goods> {

}
