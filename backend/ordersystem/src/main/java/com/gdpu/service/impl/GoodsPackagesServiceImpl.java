package com.gdpu.service.impl;

import com.gdpu.bean.GoodsPackages;
import com.gdpu.mapper.GoodsPackagesMapper;
import com.gdpu.service.GoodsPackagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
@Service
public class GoodsPackagesServiceImpl extends ServiceImpl<GoodsPackagesMapper, GoodsPackages> implements GoodsPackagesService {

}
