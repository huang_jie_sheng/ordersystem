package com.gdpu.service;

import com.gdpu.bean.Shop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
public interface ShopService extends IService<Shop> {

}
