package com.gdpu.service.impl;

import com.gdpu.bean.Packages;
import com.gdpu.mapper.PackagesMapper;
import com.gdpu.service.PackagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
@Service
public class PackagesServiceImpl extends ServiceImpl<PackagesMapper, Packages> implements PackagesService {

}
