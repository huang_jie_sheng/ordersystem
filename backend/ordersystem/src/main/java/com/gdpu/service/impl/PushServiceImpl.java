package com.gdpu.service.impl;

import com.gdpu.bean.Push;
import com.gdpu.mapper.PushMapper;
import com.gdpu.service.PushService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjs
 * @since 2021-01-28
 */
@Service
public class PushServiceImpl extends ServiceImpl<PushMapper, Push> implements PushService {

}
