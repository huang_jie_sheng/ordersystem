package com.gdpu.service;

import com.gdpu.bean.Push;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjs
 * @since 2021-01-28
 */
public interface PushService extends IService<Push> {

}
