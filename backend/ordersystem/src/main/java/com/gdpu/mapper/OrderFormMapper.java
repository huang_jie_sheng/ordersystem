package com.gdpu.mapper;

import com.gdpu.bean.OrderForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-02-14
 */
public interface OrderFormMapper extends BaseMapper<OrderForm> {

}
