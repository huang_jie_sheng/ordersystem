package com.gdpu.mapper;

import com.gdpu.bean.Count;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-02-14
 */
public interface CountMapper extends BaseMapper<Count> {

}
