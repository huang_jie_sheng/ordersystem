package com.gdpu.mapper;

import com.gdpu.bean.Leftmenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-02-22
 */
public interface LeftmenuMapper extends BaseMapper<Leftmenu> {

}
