package com.gdpu.mapper;

import com.gdpu.bean.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-02-22
 */
public interface RoleMapper extends BaseMapper<Role> {

}
