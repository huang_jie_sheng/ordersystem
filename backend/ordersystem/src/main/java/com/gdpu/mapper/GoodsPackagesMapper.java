package com.gdpu.mapper;

import com.gdpu.bean.GoodsPackages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
public interface GoodsPackagesMapper extends BaseMapper<GoodsPackages> {

}
