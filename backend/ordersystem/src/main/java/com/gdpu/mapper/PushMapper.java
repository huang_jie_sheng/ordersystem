package com.gdpu.mapper;

import com.gdpu.bean.Push;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-01-28
 */
public interface PushMapper extends BaseMapper<Push> {

}
