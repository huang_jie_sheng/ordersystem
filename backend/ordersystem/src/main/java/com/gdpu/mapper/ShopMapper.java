package com.gdpu.mapper;

import com.gdpu.bean.Shop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjs
 * @since 2021-01-31
 */
public interface ShopMapper extends BaseMapper<Shop> {

}
