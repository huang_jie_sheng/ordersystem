/*
 Navicat Premium Data Transfer

 Source Server         : mysqlConn
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : ordersystem

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 07/05/2021 21:16:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `account` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `sex` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `salt` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 0, '555', 'c9f4e1db59c1d1941ed133088a6038fe', 'hjs', 22, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (2, 0, '钱多多', '532ac00e86893901af5f0be6b704dbc7', '钱多多', 30, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (3, 1, 'zqw', '532ac00e86893901af5f0be6b704dbc7', 'zqw', 22, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (4, 2, 'lx', '532ac00e86893901af5f0be6b704dbc7', 'lx', 22, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (5, 3, 'cym', '532ac00e86893901af5f0be6b704dbc7', 'cym', 22, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (6, 3, '郑创坚', '4614066f15c698fea1ce147bd5e19694', '郑创坚', 22, '男', '04A93C74C8294AA09A8B974FD1F4ECBB');
INSERT INTO `admin` VALUES (7, 2, '老干妈', '532ac00e86893901af5f0be6b704dbc7', '老干妈', 60, '女', '04A93C74C8294AA09A8B974FD1F4ECBB');

-- ----------------------------
-- Table structure for count
-- ----------------------------
DROP TABLE IF EXISTS `count`;
CREATE TABLE `count`  (
  `count_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NULL DEFAULT NULL,
  `shop_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`count_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of count
-- ----------------------------
INSERT INTO `count` VALUES (1, 49, 6);

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `packages` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_num` int(11) NULL DEFAULT NULL,
  `menu_num` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (0, -1, NULL, NULL, NULL, 1, NULL, 41, 9);
INSERT INTO `goods` VALUES (1, 0, '订单管理系统', NULL, NULL, 1, NULL, 41, 9);
INSERT INTO `goods` VALUES (2, 1, '椰子系列', 'https://s3.ax1x.com/2021/01/31/yAcE9K.png', NULL, 1, NULL, 4, 0);
INSERT INTO `goods` VALUES (3, 1, '新品鲜果撞柠茶', 'https://s3.ax1x.com/2021/01/31/yAcZcD.png', NULL, 1, NULL, 4, 0);
INSERT INTO `goods` VALUES (4, 1, '雪顶系列', 'https://s3.ax1x.com/2021/01/31/yAcnnH.png', NULL, 1, NULL, 2, 0);
INSERT INTO `goods` VALUES (5, 1, '森享鲜果茶', 'https://s3.ax1x.com/2021/01/31/yAcKHA.png', NULL, 1, NULL, 6, 0);
INSERT INTO `goods` VALUES (6, 1, '饼干系列', 'https://s3.ax1x.com/2021/01/31/yAclNt.png', NULL, 1, NULL, 2, 0);
INSERT INTO `goods` VALUES (7, 1, '森芝味果茶', 'https://s3.ax1x.com/2021/01/31/yAc14P.png', NULL, 1, NULL, 6, 0);
INSERT INTO `goods` VALUES (8, 1, '奶茶研究所', 'https://s3.ax1x.com/2021/01/31/yAc89f.png', NULL, 1, NULL, 7, 0);
INSERT INTO `goods` VALUES (9, 1, '鲜果益力多', 'https://s3.ax1x.com/2021/01/31/yAcG38.png', NULL, 1, NULL, 6, 0);
INSERT INTO `goods` VALUES (10, 1, '森芝味茗茶', 'https://s3.ax1x.com/2021/01/31/yAcJgS.png', NULL, 1, NULL, 4, 0);
INSERT INTO `goods` VALUES (11, 2, '椰椰芒脆脆冰', 'https://s3.ax1x.com/2021/01/31/yAcUBj.png', 25, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (12, 2, '椰椰咖啡', 'https://s3.ax1x.com/2021/01/31/yAcaHs.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (13, 2, 'MINI鲜椰冻', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 9, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (14, 2, '椰椰西瓜冰', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 20, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (15, 3, '草莓撞柠茶', 'https://s3.ax1x.com/2021/01/31/yAcB40.png', 20, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (16, 3, '风梨撞柠茶', 'https://s3.ax1x.com/2021/01/31/yAcrCV.png', 20, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (17, 3, '撞撞柠茶', 'https://s3.ax1x.com/2021/01/31/yAcs3T.png', 18, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (18, 3, '葡萄撞柠茶', 'https://s3.ax1x.com/2021/01/31/yAcgu4.png', 20, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (19, 4, '奇兰鲜奶茶雪顶', 'https://s3.ax1x.com/2021/01/31/yAc2DJ.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (20, 4, '白桃鲜奶茶雪顶', 'https://s3.ax1x.com/2021/01/31/yAcRb9.png', 20, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (21, 5, '巴贝水果茶', 'https://s3.ax1x.com/2021/01/31/yAcfER.png', 24, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (22, 5, '森享西柚', 'https://s3.ax1x.com/2021/01/31/yAc44x.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (23, 5, '森享橙橙', 'https://s3.ax1x.com/2021/01/31/yAcIC6.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (24, 5, '森猫果茶', 'https://s3.ax1x.com/2021/01/31/yAcTgO.png', 17, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (25, 5, '芒芒甘露', 'https://s3.ax1x.com/2021/01/31/yAcqDH.png', 24, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (26, 5, '莓莓花果茶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (27, 6, '超浓豆乳鲜奶茶', 'https://s3.ax1x.com/2021/01/31/yAcjUI.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (28, 6, '厚芋泥鲜奶茶', 'https://s3.ax1x.com/2021/01/31/yAczPP.png', 24, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (29, 7, '森芝味葡萄', 'https://s3.ax1x.com/2021/01/31/yAgS8f.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (30, 7, '森芝味草莓', 'https://s3.ax1x.com/2021/01/31/yAg9xS.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (31, 7, '森芝味草莓', 'https://s3.ax1x.com/2021/01/31/yAgPKg.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (32, 7, '森芝味芒芒', 'https://s3.ax1x.com/2021/01/31/yAgFbj.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (33, 7, '森芝味三莓', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 29, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (34, 7, '森芝味蓝蓝莓', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (35, 8, '脏脏茶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 26, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (36, 8, '酱酱金珠珠奶茶', 'https://s3.ax1x.com/2021/01/31/yAgtG6.png', 17, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (37, 8, '森芝味奶茶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 17, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (38, 8, '金珠珠奶茶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 15, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (39, 8, '阿华田布蕾鲜奶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 24, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (40, 8, '红豆鲜奶冰', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 17, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (41, 8, '布蕾抹茶红豆冰', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (42, 9, '百香果益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 19, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (43, 9, '柠檬益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 17, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (44, 9, '橙子益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 21, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (45, 9, '西柚益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 21, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (46, 9, '草莓益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (47, 9, '蓝蓝莓益力多', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 22, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (48, 10, '森芝味抹茶', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 18, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (49, 10, '森芝味云绿', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 16, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (50, 10, '森芝味四季春', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 18, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (51, 10, '森芝味白桃乌龙', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 18, 1, NULL, 0, 0);
INSERT INTO `goods` VALUES (52, 8, '插入测试', 'https://s3.ax1x.com/2021/01/31/yAcwEn.png', 88, 0, NULL, 0, 0);

-- ----------------------------
-- Table structure for goods_packages
-- ----------------------------
DROP TABLE IF EXISTS `goods_packages`;
CREATE TABLE `goods_packages`  (
  `goods_id` int(11) NULL DEFAULT NULL,
  `package_id` int(11) NULL DEFAULT NULL,
  `package_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_packages
-- ----------------------------
INSERT INTO `goods_packages` VALUES (11, 1, '温度');
INSERT INTO `goods_packages` VALUES (11, 2, '糖度');
INSERT INTO `goods_packages` VALUES (11, 3, '加料');
INSERT INTO `goods_packages` VALUES (12, 1, '温度');
INSERT INTO `goods_packages` VALUES (12, 2, '糖度');
INSERT INTO `goods_packages` VALUES (12, 3, '加料');
INSERT INTO `goods_packages` VALUES (13, 1, '温度');
INSERT INTO `goods_packages` VALUES (13, 2, '糖度');
INSERT INTO `goods_packages` VALUES (13, 3, '加料');
INSERT INTO `goods_packages` VALUES (14, 1, '温度');
INSERT INTO `goods_packages` VALUES (14, 2, '糖度');
INSERT INTO `goods_packages` VALUES (14, 3, '加料');
INSERT INTO `goods_packages` VALUES (15, 1, '温度');
INSERT INTO `goods_packages` VALUES (15, 2, '糖度');
INSERT INTO `goods_packages` VALUES (15, 3, '加料');
INSERT INTO `goods_packages` VALUES (16, 1, '温度');
INSERT INTO `goods_packages` VALUES (16, 2, '糖度');
INSERT INTO `goods_packages` VALUES (16, 3, '加料');
INSERT INTO `goods_packages` VALUES (17, 1, '温度');
INSERT INTO `goods_packages` VALUES (17, 2, '糖度');
INSERT INTO `goods_packages` VALUES (17, 3, '加料');
INSERT INTO `goods_packages` VALUES (18, 1, '温度');
INSERT INTO `goods_packages` VALUES (18, 2, '糖度');
INSERT INTO `goods_packages` VALUES (18, 3, '加料');
INSERT INTO `goods_packages` VALUES (19, 1, '温度');
INSERT INTO `goods_packages` VALUES (19, 2, '糖度');
INSERT INTO `goods_packages` VALUES (19, 3, '加料');
INSERT INTO `goods_packages` VALUES (20, 1, '温度');
INSERT INTO `goods_packages` VALUES (20, 2, '糖度');
INSERT INTO `goods_packages` VALUES (20, 3, '加料');
INSERT INTO `goods_packages` VALUES (21, 1, '温度');
INSERT INTO `goods_packages` VALUES (21, 2, '糖度');
INSERT INTO `goods_packages` VALUES (21, 3, '加料');
INSERT INTO `goods_packages` VALUES (22, 1, '温度');
INSERT INTO `goods_packages` VALUES (22, 2, '糖度');
INSERT INTO `goods_packages` VALUES (22, 3, '加料');
INSERT INTO `goods_packages` VALUES (23, 1, '温度');
INSERT INTO `goods_packages` VALUES (23, 2, '糖度');
INSERT INTO `goods_packages` VALUES (23, 3, '加料');
INSERT INTO `goods_packages` VALUES (24, 1, '温度');
INSERT INTO `goods_packages` VALUES (24, 2, '糖度');
INSERT INTO `goods_packages` VALUES (24, 3, '加料');
INSERT INTO `goods_packages` VALUES (25, 1, '温度');
INSERT INTO `goods_packages` VALUES (25, 2, '糖度');
INSERT INTO `goods_packages` VALUES (25, 3, '加料');
INSERT INTO `goods_packages` VALUES (26, 1, '温度');
INSERT INTO `goods_packages` VALUES (26, 2, '糖度');
INSERT INTO `goods_packages` VALUES (26, 3, '加料');
INSERT INTO `goods_packages` VALUES (27, 1, '温度');
INSERT INTO `goods_packages` VALUES (27, 2, '糖度');
INSERT INTO `goods_packages` VALUES (27, 3, '加料');
INSERT INTO `goods_packages` VALUES (28, 1, '温度');
INSERT INTO `goods_packages` VALUES (28, 2, '糖度');
INSERT INTO `goods_packages` VALUES (28, 3, '加料');
INSERT INTO `goods_packages` VALUES (29, 1, '温度');
INSERT INTO `goods_packages` VALUES (29, 2, '糖度');
INSERT INTO `goods_packages` VALUES (29, 3, '加料');
INSERT INTO `goods_packages` VALUES (30, 1, '温度');
INSERT INTO `goods_packages` VALUES (30, 2, '糖度');
INSERT INTO `goods_packages` VALUES (30, 3, '加料');
INSERT INTO `goods_packages` VALUES (31, 1, '温度');
INSERT INTO `goods_packages` VALUES (31, 2, '糖度');
INSERT INTO `goods_packages` VALUES (31, 3, '加料');
INSERT INTO `goods_packages` VALUES (32, 1, '温度');
INSERT INTO `goods_packages` VALUES (32, 2, '糖度');
INSERT INTO `goods_packages` VALUES (32, 3, '加料');
INSERT INTO `goods_packages` VALUES (33, 1, '温度');
INSERT INTO `goods_packages` VALUES (33, 2, '糖度');
INSERT INTO `goods_packages` VALUES (33, 3, '加料');

-- ----------------------------
-- Table structure for goods_shop
-- ----------------------------
DROP TABLE IF EXISTS `goods_shop`;
CREATE TABLE `goods_shop`  (
  `shop_id` int(11) NULL DEFAULT NULL,
  `goods_id` int(11) NULL DEFAULT NULL,
  `available` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_shop
-- ----------------------------
INSERT INTO `goods_shop` VALUES (1, 6, 1);
INSERT INTO `goods_shop` VALUES (1, 2, 1);
INSERT INTO `goods_shop` VALUES (1, 3, 1);
INSERT INTO `goods_shop` VALUES (1, 16, 1);
INSERT INTO `goods_shop` VALUES (1, 9, 1);
INSERT INTO `goods_shop` VALUES (1, 7, 1);
INSERT INTO `goods_shop` VALUES (1, 1, 1);
INSERT INTO `goods_shop` VALUES (1, 5, 1);
INSERT INTO `goods_shop` VALUES (1, 8, 1);
INSERT INTO `goods_shop` VALUES (1, 4, 1);
INSERT INTO `goods_shop` VALUES (1, 10, 1);
INSERT INTO `goods_shop` VALUES (1, 11, 1);
INSERT INTO `goods_shop` VALUES (1, 12, 1);
INSERT INTO `goods_shop` VALUES (1, 13, 1);
INSERT INTO `goods_shop` VALUES (1, 14, 1);
INSERT INTO `goods_shop` VALUES (1, 15, 1);
INSERT INTO `goods_shop` VALUES (1, 17, 1);
INSERT INTO `goods_shop` VALUES (1, 18, 1);
INSERT INTO `goods_shop` VALUES (1, 19, 1);
INSERT INTO `goods_shop` VALUES (1, 20, 1);
INSERT INTO `goods_shop` VALUES (1, 21, 1);
INSERT INTO `goods_shop` VALUES (1, 22, 1);
INSERT INTO `goods_shop` VALUES (1, 23, 1);
INSERT INTO `goods_shop` VALUES (1, 24, 1);
INSERT INTO `goods_shop` VALUES (1, 25, 1);
INSERT INTO `goods_shop` VALUES (1, 26, 1);
INSERT INTO `goods_shop` VALUES (1, 27, 1);
INSERT INTO `goods_shop` VALUES (1, 28, 1);
INSERT INTO `goods_shop` VALUES (1, 29, 1);
INSERT INTO `goods_shop` VALUES (1, 30, 1);
INSERT INTO `goods_shop` VALUES (1, 31, 1);
INSERT INTO `goods_shop` VALUES (1, 32, 1);
INSERT INTO `goods_shop` VALUES (1, 33, 1);
INSERT INTO `goods_shop` VALUES (1, 34, 1);
INSERT INTO `goods_shop` VALUES (1, 35, 1);
INSERT INTO `goods_shop` VALUES (1, 36, 1);
INSERT INTO `goods_shop` VALUES (1, 37, 1);
INSERT INTO `goods_shop` VALUES (1, 38, 1);
INSERT INTO `goods_shop` VALUES (1, 39, 1);
INSERT INTO `goods_shop` VALUES (1, 40, 1);
INSERT INTO `goods_shop` VALUES (1, 41, 1);
INSERT INTO `goods_shop` VALUES (1, 42, 1);
INSERT INTO `goods_shop` VALUES (1, 43, 1);
INSERT INTO `goods_shop` VALUES (1, 44, 1);
INSERT INTO `goods_shop` VALUES (1, 45, 1);
INSERT INTO `goods_shop` VALUES (1, 46, 1);
INSERT INTO `goods_shop` VALUES (1, 47, 1);
INSERT INTO `goods_shop` VALUES (1, 48, 1);
INSERT INTO `goods_shop` VALUES (1, 49, 1);
INSERT INTO `goods_shop` VALUES (1, 50, 1);
INSERT INTO `goods_shop` VALUES (1, 51, 1);
INSERT INTO `goods_shop` VALUES (3, 1, 1);
INSERT INTO `goods_shop` VALUES (3, 2, 1);
INSERT INTO `goods_shop` VALUES (3, 11, 1);
INSERT INTO `goods_shop` VALUES (3, 12, 1);
INSERT INTO `goods_shop` VALUES (3, 13, 1);
INSERT INTO `goods_shop` VALUES (3, 14, 1);
INSERT INTO `goods_shop` VALUES (1, 0, 1);
INSERT INTO `goods_shop` VALUES (3, 0, 1);
INSERT INTO `goods_shop` VALUES (2, 0, 1);
INSERT INTO `goods_shop` VALUES (2, 1, 1);
INSERT INTO `goods_shop` VALUES (2, 11, 1);
INSERT INTO `goods_shop` VALUES (2, 12, 1);
INSERT INTO `goods_shop` VALUES (2, 13, 1);
INSERT INTO `goods_shop` VALUES (2, 14, 1);
INSERT INTO `goods_shop` VALUES (2, 2, 1);
INSERT INTO `goods_shop` VALUES (2, 4, 1);
INSERT INTO `goods_shop` VALUES (2, 19, 1);

-- ----------------------------
-- Table structure for leftmenu
-- ----------------------------
DROP TABLE IF EXISTS `leftmenu`;
CREATE TABLE `leftmenu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `href` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `open` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of leftmenu
-- ----------------------------
INSERT INTO `leftmenu` VALUES (1, 0, '仓库管理系统', '&#xe68e;', '', 1);
INSERT INTO `leftmenu` VALUES (3, 1, '基础管理', '&#xe857;', '', 0);
INSERT INTO `leftmenu` VALUES (4, 1, '系统管理', '&#xe614;', '', 0);
INSERT INTO `leftmenu` VALUES (17, 4, '角色管理', '&#xe650;', '/sys/toRoleManager', 0);
INSERT INTO `leftmenu` VALUES (18, 4, '用户管理', '&#xe612;', '/sys/toUserManager', 0);
INSERT INTO `leftmenu` VALUES (127, 3, '商店设置', '&#xe658;', '/sys/toShopManager', 0);
INSERT INTO `leftmenu` VALUES (128, 3, '商品设置', '&#xe698;', '/sys/toGoodsManager', 0);

-- ----------------------------
-- Table structure for order_form
-- ----------------------------
DROP TABLE IF EXISTS `order_form`;
CREATE TABLE `order_form`  (
  `order_id` int(11) NULL DEFAULT NULL,
  `open_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `goods_id` int(11) NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  `order_time` datetime(0) NULL DEFAULT NULL,
  `shop_id` int(11) NULL DEFAULT NULL,
  `price_sum` int(11) NULL DEFAULT NULL,
  `finish` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_form
-- ----------------------------
INSERT INTO `order_form` VALUES (1, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 3, '2021-02-14 06:18:01', 1, 252, 1);
INSERT INTO `order_form` VALUES (1, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 3, '2021-02-14 06:18:01', 1, 252, 1);
INSERT INTO `order_form` VALUES (1, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 3, '2021-02-14 06:18:01', 1, 252, 1);
INSERT INTO `order_form` VALUES (2, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 23, 2, '2021-02-14 06:20:13', 1, 86, 1);
INSERT INTO `order_form` VALUES (2, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 24, 3, '2021-02-14 06:20:13', 1, 86, 1);
INSERT INTO `order_form` VALUES (3, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13525223333', 9, 1, '2021-02-14 06:27:25', 1, 32, 1);
INSERT INTO `order_form` VALUES (3, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13525223333', 10, 1, '2021-02-14 06:27:25', 1, 32, 1);
INSERT INTO `order_form` VALUES (4, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13211661611', 9, 1, '2021-02-14 11:00:57', 3, 54, 1);
INSERT INTO `order_form` VALUES (4, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13211661611', 10, 1, '2021-02-14 11:00:57', 3, 54, 1);
INSERT INTO `order_form` VALUES (5, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '18955666334', 48, 2, '2021-02-14 11:02:29', 1, 60, 1);
INSERT INTO `order_form` VALUES (5, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '18955666334', 47, 1, '2021-02-14 11:02:29', 1, 60, 1);
INSERT INTO `order_form` VALUES (6, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 17, 1, '2021-02-14 11:12:20', 1, 42, 1);
INSERT INTO `order_form` VALUES (6, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 16, 1, '2021-02-14 11:12:20', 1, 42, 1);
INSERT INTO `order_form` VALUES (7, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13545555555', 18, 1, '2021-02-14 11:17:08', 1, 43, 1);
INSERT INTO `order_form` VALUES (7, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13545555555', 17, 1, '2021-02-14 11:17:08', 1, 43, 1);
INSERT INTO `order_form` VALUES (8, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13426666666', 39, 1, '2021-02-14 11:19:38', 1, 41, 1);
INSERT INTO `order_form` VALUES (8, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13426666666', 38, 1, '2021-02-14 11:19:38', 1, 41, 1);
INSERT INTO `order_form` VALUES (9, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13566613322', 22, 1, '2021-02-14 11:29:00', 1, 38, 1);
INSERT INTO `order_form` VALUES (9, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13566613322', 21, 1, '2021-02-14 11:29:00', 1, 38, 1);
INSERT INTO `order_form` VALUES (10, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13566656322', 9, 2, '2021-02-14 11:34:46', 3, 96, 1);
INSERT INTO `order_form` VALUES (10, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13566656322', 10, 2, '2021-02-14 11:34:46', 3, 96, 1);
INSERT INTO `order_form` VALUES (11, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 47, 1, '2021-02-14 11:39:32', 1, 16, 1);
INSERT INTO `order_form` VALUES (12, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 10, 1, '2021-02-14 11:40:10', 1, 22, 1);
INSERT INTO `order_form` VALUES (13, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 9, 1, '2021-02-14 11:40:59', 3, 47, 1);
INSERT INTO `order_form` VALUES (13, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-02-14 11:40:59', 3, 47, 1);
INSERT INTO `order_form` VALUES (14, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13169999999', 47, 1, '2021-02-16 01:59:37', 1, 34, 1);
INSERT INTO `order_form` VALUES (14, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13169999999', 48, 1, '2021-02-16 01:59:37', 1, 34, 1);
INSERT INTO `order_form` VALUES (15, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544465689', 9, 1, '2021-03-11 19:39:58', 1, 47, 1);
INSERT INTO `order_form` VALUES (15, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544465689', 10, 1, '2021-03-11 19:39:58', 1, 47, 1);
INSERT INTO `order_form` VALUES (16, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-03-12 02:17:07', 1, 76, 1);
INSERT INTO `order_form` VALUES (16, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-03-12 02:17:07', 1, 76, 1);
INSERT INTO `order_form` VALUES (16, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 13, 1, '2021-03-12 02:17:07', 1, 76, 1);
INSERT INTO `order_form` VALUES (16, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 16, 1, '2021-03-12 02:17:07', 1, 76, 1);
INSERT INTO `order_form` VALUES (17, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564519', 16, 1, '2021-04-23 12:24:09', 1, 20, 1);
INSERT INTO `order_form` VALUES (18, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544454444', 12, 1, '2021-04-23 21:22:28', 1, 31, 1);
INSERT INTO `order_form` VALUES (18, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544454444', 13, 1, '2021-04-23 21:22:28', 1, 31, 1);
INSERT INTO `order_form` VALUES (19, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-23 21:23:16', 1, 47, 1);
INSERT INTO `order_form` VALUES (19, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-23 21:23:16', 1, 47, 1);
INSERT INTO `order_form` VALUES (20, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-23 21:26:53', 1, 65, 1);
INSERT INTO `order_form` VALUES (20, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 2, '2021-04-23 21:26:53', 1, 65, 1);
INSERT INTO `order_form` VALUES (21, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-28 17:56:12', 1, 47, 1);
INSERT INTO `order_form` VALUES (21, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-28 17:56:12', 1, 47, 1);
INSERT INTO `order_form` VALUES (22, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544446666', 11, 1, '2021-04-28 19:00:20', 1, 47, 1);
INSERT INTO `order_form` VALUES (22, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544446666', 12, 1, '2021-04-28 19:00:20', 1, 47, 1);
INSERT INTO `order_form` VALUES (23, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 2, '2021-04-29 20:13:40', 1, 60, 1);
INSERT INTO `order_form` VALUES (23, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-04-29 20:13:40', 1, 60, 1);
INSERT INTO `order_form` VALUES (24, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-29 20:20:53', 1, 47, 1);
INSERT INTO `order_form` VALUES (24, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-29 20:20:53', 1, 47, 1);
INSERT INTO `order_form` VALUES (25, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-29 20:23:21', 1, 56, 1);
INSERT INTO `order_form` VALUES (25, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-29 20:23:21', 1, 56, 1);
INSERT INTO `order_form` VALUES (25, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 13, 1, '2021-04-29 20:23:21', 1, 56, 1);
INSERT INTO `order_form` VALUES (26, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 49, 1, '2021-04-29 22:26:01', 1, 34, 1);
INSERT INTO `order_form` VALUES (26, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 48, 1, '2021-04-29 22:26:01', 1, 34, 1);
INSERT INTO `order_form` VALUES (27, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13566666666', 14, 1, '2021-04-29 22:41:09', 1, 20, 1);
INSERT INTO `order_form` VALUES (28, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-04-29 22:48:06', 1, 40, 1);
INSERT INTO `order_form` VALUES (28, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 22:48:06', 1, 40, 1);
INSERT INTO `order_form` VALUES (29, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 50, 1, '2021-04-29 22:49:10', 1, 34, 1);
INSERT INTO `order_form` VALUES (29, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 49, 1, '2021-04-29 22:49:10', 1, 34, 1);
INSERT INTO `order_form` VALUES (30, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 22:51:00', 1, 20, 1);
INSERT INTO `order_form` VALUES (31, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 22:53:24', 1, 44, 1);
INSERT INTO `order_form` VALUES (31, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 21, 1, '2021-04-29 22:53:24', 1, 44, 1);
INSERT INTO `order_form` VALUES (32, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 42, 1, '2021-04-29 23:02:04', 1, 36, 1);
INSERT INTO `order_form` VALUES (32, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 43, 1, '2021-04-29 23:02:04', 1, 36, 1);
INSERT INTO `order_form` VALUES (33, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 13, 1, '2021-04-29 23:05:31', 1, 29, 1);
INSERT INTO `order_form` VALUES (33, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 23:05:31', 1, 29, 1);
INSERT INTO `order_form` VALUES (34, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 23:08:25', 1, 40, 1);
INSERT INTO `order_form` VALUES (34, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-04-29 23:09:57', 1, 40, 1);
INSERT INTO `order_form` VALUES (35, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 36, 1, '2021-04-29 23:18:23', 1, 43, 1);
INSERT INTO `order_form` VALUES (35, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 35, 1, '2021-04-29 23:18:23', 1, 43, 1);
INSERT INTO `order_form` VALUES (36, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-04-29 23:21:49', 1, 49, 1);
INSERT INTO `order_form` VALUES (36, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-04-29 23:21:49', 1, 49, 1);
INSERT INTO `order_form` VALUES (36, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 13, 1, '2021-04-29 23:21:49', 1, 49, 1);
INSERT INTO `order_form` VALUES (37, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 16, 1, '2021-04-30 04:34:04', 1, 38, 1);
INSERT INTO `order_form` VALUES (37, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 17, 1, '2021-04-30 04:34:04', 1, 38, 1);
INSERT INTO `order_form` VALUES (38, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-30 04:36:02', 1, 47, 1);
INSERT INTO `order_form` VALUES (38, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-30 04:36:02', 1, 47, 1);
INSERT INTO `order_form` VALUES (39, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 50, 1, '2021-04-30 04:38:46', 1, 18, 1);
INSERT INTO `order_form` VALUES (40, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13555555555', 18, 1, '2021-04-30 04:40:13', 1, 20, 1);
INSERT INTO `order_form` VALUES (41, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-04-30 04:41:44', 3, 47, 1);
INSERT INTO `order_form` VALUES (41, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-04-30 04:41:44', 3, 47, 1);
INSERT INTO `order_form` VALUES (42, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-04-30 04:44:00', 3, 20, 1);
INSERT INTO `order_form` VALUES (43, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 21, 1, '2021-05-01 04:31:17', 1, 44, 1);
INSERT INTO `order_form` VALUES (43, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 20, 1, '2021-05-01 04:31:17', 1, 44, 1);
INSERT INTO `order_form` VALUES (44, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-05-01 04:34:19', 1, 47, 1);
INSERT INTO `order_form` VALUES (44, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-05-01 04:34:25', 1, 47, 1);
INSERT INTO `order_form` VALUES (45, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 45, 1, '2021-05-01 04:46:52', 1, 42, 1);
INSERT INTO `order_form` VALUES (45, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 44, 1, '2021-05-01 04:46:52', 1, 42, 1);
INSERT INTO `order_form` VALUES (46, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 11, 1, '2021-05-01 04:47:39', 1, 47, 1);
INSERT INTO `order_form` VALUES (46, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-05-01 04:47:39', 1, 47, 1);
INSERT INTO `order_form` VALUES (47, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 14, 1, '2021-05-01 04:48:54', 1, 40, 1);
INSERT INTO `order_form` VALUES (47, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 15, 1, '2021-05-01 04:48:54', 1, 40, 1);
INSERT INTO `order_form` VALUES (48, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 12, 1, '2021-05-01 12:58:25', 1, 31, 1);
INSERT INTO `order_form` VALUES (48, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564570', 13, 1, '2021-05-01 12:58:25', 1, 31, 1);
INSERT INTO `order_form` VALUES (49, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564702', 12, 1, '2021-05-01 13:02:29', 1, 51, 1);
INSERT INTO `order_form` VALUES (49, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564702', 13, 1, '2021-05-01 13:02:29', 1, 51, 1);
INSERT INTO `order_form` VALUES (49, 'oUr-45F0qIW_yMB71V9hBF1YaARw', '13544564702', 15, 1, '2021-05-01 13:02:29', 1, 51, 1);

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages`  (
  `package_id` int(11) NOT NULL,
  `choose` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of packages
-- ----------------------------
INSERT INTO `packages` VALUES (1, '热');
INSERT INTO `packages` VALUES (1, '温');
INSERT INTO `packages` VALUES (1, '标准冰');
INSERT INTO `packages` VALUES (1, '少冰');
INSERT INTO `packages` VALUES (1, '去冰');
INSERT INTO `packages` VALUES (2, '标准糖');
INSERT INTO `packages` VALUES (2, '少糖');
INSERT INTO `packages` VALUES (2, '少少糖');
INSERT INTO `packages` VALUES (2, '无糖');
INSERT INTO `packages` VALUES (3, '加珍珠');
INSERT INTO `packages` VALUES (3, '加波霸');
INSERT INTO `packages` VALUES (3, '加椰果');

-- ----------------------------
-- Table structure for push
-- ----------------------------
DROP TABLE IF EXISTS `push`;
CREATE TABLE `push`  (
  `push_id` int(11) NOT NULL AUTO_INCREMENT,
  `navigator_url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image_url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '0为轮播类型推送，1为取餐方式，2为资讯类型推送',
  `text` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资讯的说明文本',
  PRIMARY KEY (`push_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of push
-- ----------------------------
INSERT INTO `push` VALUES (1, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/09/sMb91J.png', 0, NULL);
INSERT INTO `push` VALUES (2, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/09/sMbpp4.png', 0, NULL);
INSERT INTO `push` VALUES (3, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/28/ySTay4.png', 1, NULL);
INSERT INTO `push` VALUES (4, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/28/ySTGF0.png', 1, NULL);
INSERT INTO `push` VALUES (5, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/29/yClKG4.png', 2, '新品 |万圣节，喝点热的？');
INSERT INTO `push` VALUES (6, '/pages/buy/buy', 'https://s3.ax1x.com/2021/01/29/yClMRJ.png', 2, '会员系统升级');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `remark` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `available` int(11) NULL DEFAULT NULL,
  `createtime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (0, '区域经理', '拥有所有菜单权限', 1, '2020-06-29 00:00:00');
INSERT INTO `role` VALUES (1, '广州店店长', '广州店店长', 1, '2020-06-29 00:00:00');
INSERT INTO `role` VALUES (2, '2号经理', '能够操作2号仓库', 1, '2020-06-29 00:00:00');
INSERT INTO `role` VALUES (3, '3号经理', '能够操作3号仓库', 1, '2020-06-29 00:00:00');
INSERT INTO `role` VALUES (4, '4号经理', '4号仓库管理员', 0, '2020-07-02 14:50:14');
INSERT INTO `role` VALUES (5, '5号经理', '能够操作5号仓库', 0, '2020-06-29 00:00:00');

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop`  (
  `shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `admin_id` int(11) NULL DEFAULT NULL,
  `available` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`shop_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES (1, '广东省广州市天河区天河客运站', 1, 1);
INSERT INTO `shop` VALUES (2, '北京市海淀区中关村南大街2号', 2, 0);
INSERT INTO `shop` VALUES (3, '北京市海淀区中关村南大街5号', 3, 0);
INSERT INTO `shop` VALUES (4, '广东省番禺区大学城', 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
