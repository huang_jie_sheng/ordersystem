// 0 引入 用来发送请求的 方法 一定要把路径补全
import { request } from "../../request/index.js";
let app = getApp();
Page({
  data:{
    // 轮播图数组
    swiperList: [],
    //取餐数组
    wayList:[],
    //咨询数组
    newsList:[],
    appid:'wxdfaf253193f2837d',
    secret:'bd556b826e8f8a83cfa461422c885101',
  },
  // 页面开始加载 就会触发
  onLoad: function (options) {
    
        var that = this;
        var user=wx.getStorageSync('user') || {};  
        var userInfo=wx.getStorageSync('userInfo') || {};
        if((!user.openid || (user.expires_in || Date.now()) < (Date.now() + 600))&&(!userInfo.nickName)){ 
            
          wx.login({  
            success: function(res){ 
                if(res.code) {
                    wx.getUserInfo({
                        success: function (res) {
                            var objz={};
                            objz.avatarUrl=res.userInfo.avatarUrl;
                            objz.nickName=res.userInfo.nickName;
                            // wx.setStorageSync('userInfo', objz);//存储userInfo
                        }
                    });
                    var d=that.data;//这里存储了appid、secret、token串  
                    var l='https://api.weixin.qq.com/sns/jscode2session?appid='+d.appid+'&secret='+d.secret+'&js_code='+res.code+'&grant_type=authorization_code';  
                    wx.request({  
                        url: l,  
                        data: {},  
                        method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT  
                        // header: {}, // 设置请求的 header  
                        success: (res)=>{ 
                            //定义object的形式
                            // var obj={};
                            // obj.openid=res.data.openid;  
                            // obj.expires_in=Date.now()+res.data.expires_in;  
                            // wx.setStorageSync('user', obj);//存储openid 
                            app.globaldata.openid = res.data.openid;
                            // console.log(app.globaldata.openid)
                        }  
                    });
                }else {
                    console.log('获取用户登录态失败！' + res.errMsg)
                }          
            },
            fail:function(err){
              console.log(err)
            }  
          });
        };
    this.getSwiperList();
    this.getwayList();
    this.getNewsList();
  },

  // 获取轮播图数据
  getSwiperList(){
    request({ url: "http://127.0.0.1:8080/home/swiperdata" })
    .then(result => {
      this.setData({
        swiperList: result.data.data
        
      })
    })
  },
  //获取取餐方式数据
  getwayList(){
    request({ url: "http://127.0.0.1:8080/home/waydata" })
    .then(result => {
      this.setData({
        wayList: result.data.data
        
      })
    })
  },
  //获取资讯数据
  getNewsList(){
    request({ url: "http://127.0.0.1:8080/home/newsdata" })
    .then(result => {
      this.setData({
        newsList: result.data.data
        
      })
    })
  },
  navigate(res){
    console.log(res)
    wx.switchTab({
      
      // url: '/pages/buy/buy',
      url:this.data.swiperList[res.currentTarget.dataset.index].navigatorUrl
    })
  },
  navigate2(res){
    console.log(res)
    wx.switchTab({
      
      // url: '/pages/buy/buy',
      url:this.data.newsList[res.currentTarget.dataset.index].navigatorUrl
    })
  }
})